# Kintent catalog

## Setup
1. Clone repo `git clone`;
2. Go to dir `cd kintent`;
3. Create .env files `cp .env.example .env; cp api/.env.example api/.env; cp client/.env.example client/.env`;
4. Check all .env files and adjust it according to your needs;
5. Start server `docker-compose up -d --build`;
5.1. Sometimes (don't know why at that moment) not always seeds are running, so run `docker-compose down && docker-compose up -d`;
6. Go to client folder and run `yarn && yarn start`;
7. You should be able to see app on http://localhost:3001

## Demo logins
1. jd@scrubs.com / eagle
2. cox@scrubs.com / fury
