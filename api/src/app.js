const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const ErrorHandlerMiddleware = require('./middleware/error-handler.middleware');

const authRouter = require('./routes/auth');
const productsRouter = require('./routes/products');
const ordersRouter = require('./routes/orders');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// CORS
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,PATCH,DELETE");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization, x-auth-token"
    );
    next();
});

require('./models').sequelize.sync();

app.use('/auth', authRouter);
app.use('/products', productsRouter);
app.use('/orders', ordersRouter);

// error handler
app.use(ErrorHandlerMiddleware.handle);

module.exports = app;
