class Helper {
    toCents(value) {
        return typeof value === 'number' ? Math.round(100 * value) : null;
    }

    fromCents(value) {
        return typeof value === 'number' ? value / 100 : null;
    }
}



class Singleton {
    constructor() {
        if (!Singleton.instance) {
            Singleton.instance = new Helper();
        }
    }

    getInstance() {
        return Singleton.instance;
    }
}

module.exports = new Singleton().getInstance();
