const AuthService = require('../services/auth.service');
const HttpStatuses = require('../common/http-statuses');

class AuthController {
    async login(req, res, next) {
        const { email, password } = req.body;

        try {
            const accessToken = await AuthService.login(email, password);

            res.status(HttpStatuses.CREATED);
            res.json({ access_token: accessToken });
        } catch (e) {
            next(e);
        }
    }
}

module.exports = new AuthController();
