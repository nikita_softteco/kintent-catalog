const OrderService = require('../services/order.service');

const HttpStatuses = require('../common/http-statuses');

class OrderController {
    async findAll(req, res, next) {
        try {
            const orders = await OrderService.findAll(req.user.id);

            res.json({ orders });
        } catch (e) {
            next(e);
        }
    }

    async find(req, res, next) {
        try {
            const order = await OrderService.findForUser(req.user.id, req.params.id);

            res.json({ order });
        } catch (e) {
            next(e);
        }
    }

    async create(req, res, next) {
        try {
            const order = await OrderService.create(req.user.id, req.body);

            res.status(HttpStatuses.CREATED)
            res.json({ order });
        } catch (e) {
            next(e);
        }
    }

    async cancel(req, res, next) {
        try {
            if (!!(await OrderService.cancel(req.user.id, req.params.id))) {
                res.json({ status: 'ok' });
            }
        } catch (e) {
            next(e);
        }
    }
}

module.exports = new OrderController();
