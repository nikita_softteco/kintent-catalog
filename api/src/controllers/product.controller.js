const ProductService = require('../services/product.service');

class ProductController {
    async findAll(req, res, next) {
        try {
            const products = await ProductService.findAll(req.query.search);

            res.json({ products });
        } catch (e) {
            next(e);
        }
    }
}

module.exports = new ProductController();
