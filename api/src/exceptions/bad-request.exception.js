const HttpCodes = require('../common/http-statuses');

class BadRequestException extends Error {
    statusCode;
    message;

    constructor(message = '') {
        super();
        this.statusCode = HttpCodes.BAD_REQUEST;
        this.message = message;
    }
}

module.exports = BadRequestException;
