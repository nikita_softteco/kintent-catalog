const HttpCodes = require('../common/http-statuses');

class EnvVarNotSetException extends Error {
    statusCode;
    message;

    constructor() {
        super();
        this.statusCode = HttpCodes.SERVER_ERROR;
        this.message = 'Environment variable not set';
    }
}

module.exports = EnvVarNotSetException;
