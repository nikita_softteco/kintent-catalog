const HttpCodes = require('../common/http-statuses');

class InvalidTokenException extends Error {
    statusCode;
    message;

    constructor() {
        super();
        this.statusCode = HttpCodes.FORBIDDEN;
        this.message = 'Forbidden.';
    }
}

module.exports = InvalidTokenException;
