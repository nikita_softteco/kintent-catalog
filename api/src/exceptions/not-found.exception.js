const HttpCodes = require('../common/http-statuses');

class NotFoundException extends Error {
    statusCode;
    message;

    constructor(message) {
        super();
        this.statusCode = HttpCodes.NOT_FOUND;
        this.message = message;
    }
}

module.exports = NotFoundException;
