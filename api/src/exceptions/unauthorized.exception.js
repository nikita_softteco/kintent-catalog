const HttpCodes = require('../common/http-statuses');

class UnauthorizedException extends Error {
    statusCode;
    message;

    constructor(message) {
        super();
        this.statusCode = HttpCodes.UNAUTHORIZED;
        this.message = message;
    }
}

module.exports = UnauthorizedException;
