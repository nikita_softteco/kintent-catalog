const AuthService = require('../services/auth.service');

const UnauthorizedException = require('../exceptions/unauthorized.exception');

class AuthMiddleware {
    verify(req, res, next) {
        const authHeader = req.headers.authorization;

        if (authHeader) {
            const token = authHeader.split(' ')[1];

            if (AuthService.verify(token)) {
                req.user = AuthService.getUserFromToken(token);
                next();
            }
        } else {
            throw new UnauthorizedException('Login first.');
        }
    }
}

module.exports = new AuthMiddleware();
