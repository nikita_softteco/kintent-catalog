const HttpStatuses = require('../common/http-statuses');

class ErrorHandlerMiddleware {
    handle(err, req, res, _next) {
        const code = err.statusCode || HttpStatuses.SERVER_ERROR;
        const response = {
            statusCode: code,
            message: err.message || 'Something went wrong',
        };

        res.status(code);
        res.json(response);
    }
}

module.exports = new ErrorHandlerMiddleware();
