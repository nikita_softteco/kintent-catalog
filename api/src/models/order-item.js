'use strict';

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class OrderItem extends Model {
    static associate(models) {
      this.belongsTo(models.Order, {
        as: 'order',
        foreignKey: 'order_id'
      });
      this.belongsTo(models.Product, {
        as: 'product',
        foreignKey: 'product_id'
      })
    }
  };

  OrderItem.init({
    order_id: {
      type: DataTypes.INTEGER,
      unsigned: true,
      allowNull: false
    },
    product_id: {
      type: DataTypes.INTEGER,
      unsigned: true,
      allowNull: false
    },
    quantity: {
      type: DataTypes.INTEGER,
      unsigned: true,
      allowNull: false
    },
  }, {
    sequelize,
    modelName: 'OrderItem',
    underscored: true,
  });

  return OrderItem;
};
