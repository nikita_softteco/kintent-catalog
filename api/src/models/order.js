'use strict';

const Helper = require('../common/helper');

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Order extends Model {
    static associate(models) {
      this.hasMany(models.OrderItem, {
        as: 'order_items',
        foreignKey: 'order_id'
      });
      this.belongsTo(models.User);
    }
  };

  Order.init({
    user_id: {
      type: DataTypes.INTEGER,
      unsigned: true,
      allowNull: false
    },
    total_sum: {
      type: DataTypes.INTEGER,
      get() {
        return Helper.fromCents(this.getDataValue('total_sum'));
      },
      set(value) {
        this.setDataValue('total_sum', Helper.toCents(value));
      }
    },
    status: {
      type: DataTypes.ENUM,
      values: ['IN_PROGRESS', 'PAID', 'CANCELED']
    }
  }, {
    sequelize,
    modelName: 'Order',
    underscored: true,
  });

  return Order;
};
