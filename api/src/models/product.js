'use strict';

const Helper = require('../common/helper');

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Product extends Model {};

  Product.init({
    title: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: DataTypes.STRING,
    price: {
      type: DataTypes.INTEGER,
      unsigned: true,
      allowNull: false,
      get() {
        return Helper.fromCents(this.getDataValue('price'));
      },
    },
    image: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Product',
    underscored: true,
  });

  return Product;
};
