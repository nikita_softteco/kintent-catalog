const express = require('express');
const router = express.Router();

const OrderController = require('../controllers/order.controller');
const AuthMiddleware = require('../middleware/auth.middleware');

router.get('/', AuthMiddleware.verify, OrderController.findAll);
router.get('/:id', AuthMiddleware.verify, OrderController.find);
router.post('/', AuthMiddleware.verify, OrderController.create);
router.patch('/:id', AuthMiddleware.verify, OrderController.cancel);

module.exports = router;
