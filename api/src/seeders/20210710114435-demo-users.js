'use strict';

const TABLE_NAME = 'users';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    try {
      await queryInterface.bulkInsert(TABLE_NAME, [
        {
          first_name: 'John',
          last_name: 'Dorian',
          email: 'jd@scrubs.com',
          password: 'eagle',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          first_name: 'Perry',
          last_name: 'Cox',
          email: 'cox@scrubs.com',
          password: 'fury',
          created_at: new Date(),
          updated_at: new Date()
        }
      ], {});
    } catch (e) {
      if (e.name === 'SequelizeUniqueConstraintError') {
        return;
      }
    }
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete(TABLE_NAME, null, {});
  }
};
