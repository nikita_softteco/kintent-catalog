'use strict';

const faker = require('faker');

const TABLE_NAME = 'products';

const generateDemoData = (records = 50) => {
  const data = [];
  for (let i = 0; i < records; i++) {
    data.push({
      title: faker.commerce.productName(),
      description: faker.commerce.productDescription(),
      price: faker.commerce.price(1, 99999, 0),
      image: faker.image.imageUrl(),
      created_at: new Date(),
      updated_at: new Date()
    });
  }

  return data;
};

module.exports = {
  up: async (queryInterface, Sequelize) => {
    try {
      await queryInterface.bulkInsert(TABLE_NAME, generateDemoData());
    } catch (e) {
      if (e.name === 'SequelizeUniqueConstraintError') {
        return;
      }
    }
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete(TABLE_NAME, null, {});
  }
};
