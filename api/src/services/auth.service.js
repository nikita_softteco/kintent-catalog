const jwt = require('jsonwebtoken');

const UserService = require('../services/user.service');
const ConfigService = require('../services/config.service');

const BadRequestException = require('../exceptions/bad-request.exception');
const InvalidTokenException = require('../exceptions/invalid-token.exception');

class AuthService {
    jwtSecret = ConfigService.get('JWT_SECRET');

    async login(email, password) {
        const user = await UserService.find({
            where: { email, password },
            attributes: ['id', 'first_name', 'last_name', 'email', 'created_at', 'updated_at']
        });

        if (!user) {
            throw new BadRequestException('Email or password incorrect.');
        }

        return jwt.sign(user.get({ plain: true }), this.jwtSecret);
    }

    getUserFromToken(token) {
        return jwt.verify(token, this.jwtSecret);
    }

    verify(token) {
        try {
            const user = jwt.verify(token, this.jwtSecret);

            return !!user;
        } catch (e) {
            throw new InvalidTokenException();
        }
    }
}

module.exports = new AuthService();
