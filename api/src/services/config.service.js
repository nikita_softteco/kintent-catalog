require('dotenv').config();
const EnvVarNotSetException = require('../exceptions/env-var-not-set.exception');

class ConfigService {
    get(variable) {
        if (!process.env[variable]) {
            throw new EnvVarNotSetException();
        }

        return process.env[variable];
    }

    saveGet(variable) {
        return process.env[variable];
    }
}

class Singleton {
    constructor() {
        if (!Singleton.instance) {
            Singleton.instance = new ConfigService();
        }
    }

    getInstance() {
        return Singleton.instance;
    }
}

module.exports = new Singleton().getInstance();
