const DB = require('../models');

const OrderStatusesEnum = require('../enums/order');

const Order = DB.Order;
const OrderItem = DB.OrderItem;
const Product = DB.Product;

const UnauthorizedException = require('../exceptions/unauthorized.exception');
const NotFoundException = require('../exceptions/not-found.exception');

class OrderService {
    async findAll(userId) {
        return await Order.findAll({
            where: {
                user_id: userId
            },
        })
    }

    async find(id) {
        return await Order.findByPk(id, {
            include: [{
                model: OrderItem,
                as: 'order_items',
                include: [{
                    model: Product,
                    as: 'product'
                }]
            }]
        });
    }

    async findForUser(userId, orderId) {
        const order = await this.find(orderId);

        if (!order) {
            throw new NotFoundException(`Order with id ${orderId} not found`);
        }

        if (order.user_id !== userId) {
            throw new UnauthorizedException('You are not allowed to see this order');
        }

        return order;
    }

    async create(userId, body) {
        return await Order.create({
            user_id: userId,
            total_sum: body.total_sum, // re-calculate total from order_items
            status: OrderStatusesEnum.IN_PROGRESS,
            order_items: body.order_items
        }, {
            include: [{
                model: OrderItem,
                as: 'order_items'
            }]
        });
    }

    async cancel(userId, id) {
        const order = await this.findForUser(userId, id);

        order.status = OrderStatusesEnum.CANCELED;

        return await order.save();
    }
}

module.exports = new OrderService();
