const DB = require('../models');
const Product = DB.Product;
const Op = DB.Sequelize.Op;

class ProductService {
    async findAll(searchText) {
        const whereClause = searchText ? {
            where: {
                title: {
                    [Op.iLike]: `%${searchText}%`
                }
            }
        } : {};

        return await Product.findAll(whereClause);
    }
}

module.exports = new ProductService();
