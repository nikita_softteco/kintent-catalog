const User = require('../models').User;

class UserService {
    async find(options) {
        return await User.findOne(options);
    }

    async findById(id) {
        return await User.findByPk(id);
    }
}

module.exports = new UserService();
