import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import { Cart, Login, Navbar, Order, Orders, Products } from './components';

const App = () => {
    return (
        <Router>
            <Switch>
                <Route exact path="/" component={ Login } />
                <div>
                    <Navbar />
                    <Route exact path="/products" component={ Products } />
                    <Route exact path="/cart" component={ Cart } />
                    <Route exact path="/orders" component={ Orders } />
                    <Route exact path="/orders/:id" component={ Order } />
                </div>
            </Switch>
        </Router>
    );
}

export default App;
