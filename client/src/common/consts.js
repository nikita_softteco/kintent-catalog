export const STATUSES = {
    PENDING: 'PENDING',
    SUCCESS: 'SUCCESS',
    FAILED: 'FAILED'
}

export const HTTP_METHODS = {
    GET: 'get',
    POST: 'post',
    PUT: 'put',
    PATCH: 'patch',
    DELETE: 'delete'
}

export const ORDER_STATUSES = {
    IN_PROGRESS: 'IN_PROGRESS',
    PAID: 'PAID',
    CANCELLED: 'CANCELLED'
};
