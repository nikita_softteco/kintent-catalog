export const titleCase = (str) => {
    return str
        .split('_')
        .map((word, index) => {
            return index === 0
                ? word[0].toUpperCase() + word.slice(1).toLowerCase()
                : word.toLowerCase();
        })
        .join(' ');
};

export const formatTime = (dateTime) => new Date(dateTime).toUTCString();
