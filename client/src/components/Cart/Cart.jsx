import styled from '@emotion/styled';

import { Button, Container, Grid, Typography } from '@material-ui/core';
import React from 'react';
import { connect } from 'react-redux';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import { emptyCart } from '../../store/cart/actions';
import { submitCart } from '../../store/cart/actions';
import CartItem from './CartItem/CartItem';

const BackToProductsLink = styled(Link)`
  text-decoration: none;
`;
const Title = styled(Typography)`
  margin-top: 5%;
`;
const Toolbar = styled.div`
  height: 100px;
`;
const CartDetails = styled.div`
  display: flex;
  margin-top: 10%;
  width: 100%;
  justify-content: space-between;
`;
const ResetCartButton = styled(Button)`
  min-width: 150px;
  margin-right: 1rem!important;
`;
const SubmitCartButton = styled(Button)`
  min-width: 150px;
`;

const Cart = ({ cart, count, emptyCart, submitCart }) => {
    const history = useHistory();
    const handleEmptyCart = () => emptyCart();
    const handleSubmitCart = () => {
        submitCart();
        history.push('/orders');
    };

    const renderEmptyCart = () => (
        <Typography variant="subtitle1">You have no items in your shopping cart,
            <BackToProductsLink to="/products">start adding some</BackToProductsLink>!
        </Typography>
    );

    if (!cart.order_items) return 'Loading';

    const renderCart = () => (
        <>
            <Grid container spacing={3}>
                {cart.order_items.map((item) => (
                    <Grid item xs={12} sm={4} key={item.id}>
                        <CartItem item={item}/>
                    </Grid>
                ))}
            </Grid>
            <CartDetails>
                <Typography variant="h4">Subtotal: {cart.total_sum}</Typography>
                <div>
                    <ResetCartButton size="large" type="button" variant="contained" color="secondary"
                                     onClick={handleEmptyCart}>Empty cart</ResetCartButton>
                    <SubmitCartButton onClick={handleSubmitCart} size="large" type="button" variant="contained"
                                      color="primary">Submit</SubmitCartButton>
                </div>
            </CartDetails>
        </>
    );

    return (
        <Container>
            <Toolbar/>
            <Title variant="h3" gutterBottom>Your Shopping Cart</Title>
            {!count ? renderEmptyCart() : renderCart()}
        </Container>
    );
};

const mapStateToProps = (state) => ({
    cart: state.cart.cart,
    count: state.cart.cart.order_items.length
});

const mapActionsToProps = {
    submitCart,
    emptyCart
};

export default connect(mapStateToProps, mapActionsToProps)(Cart);
