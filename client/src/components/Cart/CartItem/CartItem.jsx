import React from 'react';
import {
    Typography,
    Button,
    Card,
    CardActions,
    CardContent,
    CardMedia,
} from '@material-ui/core';
import styled from '@emotion/styled';
import { connect } from 'react-redux';
import { removeFromCart } from '../../../store/cart/actions';

const CartContent = styled(CardContent)`
    display: flex;
    justify-content: space-between;
`
const Media = styled(CardMedia)`
    height: 260px;
`
const CartAction = styled(CardActions)`
    justify-content: space-between;
`
const ButtonsWrapper = styled.div`
    display: flex;
    align-items: center;
`

const CartItem = ({ item, removeFromCart }) => {
    const handleRemoveFromCart = () => removeFromCart(item);

    return (
        <Card className="cart-item">
            <Media image={item.product.image} alt={item.product.title} />
            <CartContent>
                <Typography variant="h6">{item.product.title}</Typography>
                <Typography variant="h5">$ {item.product.price}</Typography>
            </CartContent>
            <CartAction>
                <ButtonsWrapper>
                    <Typography>&nbsp;{item.quantity}&nbsp;</Typography>
                </ButtonsWrapper>
                <Button variant="contained" type="button" color="secondary" onClick={handleRemoveFromCart}>Remove</Button>
            </CartAction>
        </Card>
    );
};

const mapActionsToProps = {
    removeFromCart
}

export default connect(null, mapActionsToProps)(CartItem);
