import styled from '@emotion/styled';
import { AppBar, Badge, IconButton, Toolbar, Typography, } from '@material-ui/core';
import { Assignment, ShoppingCart } from '@material-ui/icons';
import StorefrontIcon from '@material-ui/icons/Storefront';
import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import logo from '../../assets/logo.png';

const Navigation = styled(AppBar)`
    box-shadow: none;
    border-bottom: 1px solid rgba(0, 0, 0, 0.12);
`;
const LogoWrapper = styled(Typography)`
    flex-grow: 1;
    align-items: center;
    display: flex;
    text-decoration: none;
`;
const LogoImage = styled.img`
    margin-right: 10px;
`;
const Space = styled.div`
    flex-grow: 1;
`;
const CartButton = styled.div``;

const Navbar = ({ count }) => {
    return (
        <>
            <Navigation position="fixed" color="inherit">
                <Toolbar>
                    <LogoWrapper>
                        <LogoImage src={ logo } height="25px" alt={ process.env.REACT_APP_NAME } />
                        { process.env.REACT_APP_NAME }
                    </LogoWrapper>
                    <Space />
                    <CartButton>
                        <IconButton component={ Link } to="/products" aria-label="Products" color="inherit">
                            <StorefrontIcon />
                        </IconButton>
                    </CartButton>
                    <CartButton>
                        <IconButton component={ Link } to="/orders" aria-label="Show orders" color="inherit">
                            <Assignment />
                        </IconButton>
                    </CartButton>
                    <CartButton>
                        <IconButton component={ Link } to="/cart" aria-label="Show cart items" color="inherit">
                            <Badge badgeContent={ count } color="secondary">
                                <ShoppingCart />
                            </Badge>
                        </IconButton>
                    </CartButton>
                </Toolbar>
            </Navigation>
        </>
    );
}

const mapStateToProps = (state) => ({
    count: state.cart.cart.order_items.length
});

export default connect(mapStateToProps)(Navbar);
