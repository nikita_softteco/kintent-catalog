import styled from '@emotion/styled';
import { InputBase } from '@material-ui/core';
import { Search } from '@material-ui/icons';
import React from 'react';
import useStyles from './styles';

const SearchWrapper = styled.div`
    position: relative;
    margin-left: 0;
    margin-bottom: 2rem;
`;
const IconWrapper = styled.div`
    padding: 2px;
    height: 100%;
    position: absolute;
    pointer-events: none;
    display: flex;
    align-items: center;
    justify-content: center;
`;

const SearchField = ({ onInput, defaultValue }) => {
    const classes = useStyles();

    return (
        <SearchWrapper>
            <IconWrapper>
                <Search />
            </IconWrapper>
            <InputBase
                placeholder="Search..."
                inputProps={{ 'aria-label': 'search' }}
                value={ defaultValue }
                classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput,
                }}
                onInput={ onInput }
            />
        </SearchWrapper>
    );
}

export default SearchField;
