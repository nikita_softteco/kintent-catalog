import React from 'react';

import styled from '@emotion/styled';
import { Grid, IconButton, Paper, Typography } from '@material-ui/core';
import VisibilityIcon from '@material-ui/icons/Visibility';
import { connect } from 'react-redux';
import { useHistory } from 'react-router';
import { ORDER_STATUSES } from '../../../common/consts';
import { formatTime, titleCase } from '../../../common/utils';
import { getOrder } from '../../../store/order/actions';

const MainWrapper = styled.div`
  flex-grow: 1;
  padding: 1rem;
`;

const Status = styled(Typography)`
    padding: 10px;
    border-radius: 5px;
    background-color: ${(props) => props.status === ORDER_STATUSES.CANCELLED ? 'red' : 'blue'};
    color: white;
    float: left;
`

const Order = ({ order, getOrder }) => {
    const history = useHistory();

    const handleGetOrderDetails = () => {
        getOrder({ id: order.id });
        history.push(`/orders/${order.id}`);
    }

    return (
        <Grid item xs={12}>
            <Paper elevation={6}>
                <MainWrapper>
                    <Grid container alignItems="center">
                        <Grid item xs={2}>
                            <Status status={order.status}>
                                {titleCase(order.status)}
                            </Status>
                        </Grid>
                        <Grid item xs={8}>
                            <Typography>
                                Created @ {formatTime(order.created_at)}
                            </Typography>
                        </Grid>
                        <Grid item xs={2}>
                            <IconButton onClick={() => handleGetOrderDetails()}>
                                <VisibilityIcon />
                            </IconButton>
                        </Grid>
                    </Grid>
                </MainWrapper>
            </Paper>
        </Grid>
    );
};

const mapActionsToProps = {
    getOrder
}

export default connect(null, mapActionsToProps)(Order);
