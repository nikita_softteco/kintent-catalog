import styled from '@emotion/styled';
import { Button, Card, CardActions, CardContent, Grid, LinearProgress, Typography } from '@material-ui/core';
import React from 'react';
import { connect } from 'react-redux';
import { ORDER_STATUSES } from '../../../common/consts';
import { formatTime, titleCase } from '../../../common/utils';
import { cancelOrder } from '../../../store/order/actions';
import OrderItem from './OrderItem/OrderItem';

const Toolbar = styled.div`
  height: 50px;
`;
const MainWrapper = styled.main`
  flex-grow: 1;
  padding: 3rem;
`;
const OrderItemsWrapper = styled(Grid)`
  padding: 1rem;
  flex-grow: 1;
`

const OrderDetails = ({ order, loading, cancelOrder }) => {
    const handleCancelOrder = () => cancelOrder({ id: order.id });

    const renderLoading = () => (<LinearProgress />);
    const renderCancelButton = () => (
        <CardActions>
            <Button size="large" type="button" variant="contained" color="secondary" onClick={handleCancelOrder}>Cancel order</Button>
        </CardActions>
    );
    const renderContent = () => (
        <Grid container>
            <Grid item xs={12}>
                <Typography gutterBottom variant="h3">Order details #{order.id}</Typography>
            </Grid>
            <Grid item xs={12} md={4}>
                <Card>
                    <CardContent>
                        <Typography gutterBottom variant="h5">Total sum: $ {order.total_sum}</Typography>
                        <Typography gutterBottom variant="h5">Status: {titleCase(order.status)}</Typography>
                        <Typography gutterBottom variant="h5">Created at: {formatTime(order.created_at)}</Typography>
                    </CardContent>
                    {order.status === ORDER_STATUSES.IN_PROGRESS && renderCancelButton()}
                </Card>
            </Grid>
            <OrderItemsWrapper item xs={12} md={8}>
                <Grid container justifyContent="center" spacing={4}>
                    {order.order_items.map((orderItem) => (
                        <OrderItem orderItem={orderItem} key={orderItem.id} />
                    ))}
                </Grid>
            </OrderItemsWrapper>
        </Grid>
    );

    return (
        <MainWrapper>
            <Toolbar />
            {loading ? renderLoading() : renderContent()}
        </MainWrapper>
    );
}

const mapStateToProps = (state) => ({
    order: state.order.order,
    loading: state.order.order ? state.order.order.loading : true
});

const mapActionsToProps = {
    cancelOrder
}

export default connect(mapStateToProps, mapActionsToProps)(OrderDetails);
