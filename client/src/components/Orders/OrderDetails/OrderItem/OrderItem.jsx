import styled from '@emotion/styled';
import { Card, CardContent, CardMedia, Typography } from '@material-ui/core';
import React from 'react';
import { calculateTotalSum } from '../../../../store/utils/cart';

const CardWrapper = styled(Card)`
  display: flex;
  margin-bottom: 1rem;
`;
const Details = styled.div`
  display: flex;
  flex-direction: column;
`;
const Content = styled(CardContent)`
  flex: 1 0 auto;
  max-width: 60%;
`;
const Media = styled(CardMedia)`
    width: 151px;
`
const Info = styled.div`
  padding-left: 1rem;
  padding-bottom: 1rem;
`

const OrderItem = ({ orderItem }) => {
    return (
        <CardWrapper>
            <Details>
                <Content>
                    <Typography component="h5" variant="h5">
                        {orderItem.product.title}
                    </Typography>
                    <Typography variant="subtitle1" color="textSecondary">
                        {orderItem.product.description}
                    </Typography>
                </Content>
                <Info>
                    <Typography variant="subtitle1" color="textSecondary">
                        Price: {orderItem.product.price}
                    </Typography>
                    <Typography variant="subtitle1" color="textSecondary">
                        Quantity: {orderItem.quantity}
                    </Typography>
                    <Typography variant="subtitle1" color="textSecondary">
                        Total for item: {calculateTotalSum([orderItem])}
                    </Typography>
                </Info>
            </Details>
            <Media image={orderItem.product.image} />
        </CardWrapper>
    );
};

export default OrderItem;
