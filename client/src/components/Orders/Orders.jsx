import React from 'react';

import styled from '@emotion/styled';
import { Container, Grid } from '@material-ui/core';
import { useEffect } from 'react';
import { connect } from 'react-redux';
import { getOrders } from '../../store/orders/actions';
import Order from './Order/Order';

const Toolbar = styled.div`
  height: 50px;
`;
const MainWrapper = styled.div`
  flex-grow: 1;
  padding: 3rem;
`;

const Orders = ({orders, getOrders}) => {
    useEffect(() => getOrders(), []);

    return (
        <MainWrapper>
            <Toolbar/>
            <Container>
                <Grid container spacing={3}>
                    {orders.map((order) => (
                        <Order order={order} key={order.id} id={order.id} />
                    ))}
                </Grid>
            </Container>
        </MainWrapper>
    );
};

const mapStateToProps = (state) => ({
    orders: state.orders.orders
});

const mapActionsToProps = {
    getOrders
};

export default connect(mapStateToProps, mapActionsToProps)(Orders);
