import React from 'react';

import styled from '@emotion/styled';
import {
    Card,
    CardMedia,
    CardContent,
    CardActions,
    Typography,
    IconButton,
} from '@material-ui/core';
import { AddShoppingCart } from '@material-ui/icons';

const ProductRoot = styled(Card)`
    max-width: 100%;
`
const ProductImage = styled(CardMedia)`
    height: 0;
    padding-top: 56.25%;
`;
const ProductActions = styled(CardActions)`
    display: flex;
    justify-content: flex-end;
`;

const Product = ({ product, onAddToCart }) => {
    return (
        <ProductRoot>
            <ProductImage image={ product.image } title={ product.title } />
            <CardContent>
                <div>
                    <Typography gutterBottom variant="h6" component="h4">
                        { product.title }
                    </Typography>
                    <Typography gutterBottom variant="h6" component="h4">
                        $ { product.price }
                    </Typography>
                    <Typography variant="body2" color="textSecondary" component="p">
                        { product.description }
                    </Typography>
                </div>
            </CardContent>
            <ProductActions disableSpacing>
                <IconButton aria-label="Add to Cart" onClick={ () => onAddToCart(product, 1) }>
                    <AddShoppingCart />
                </IconButton>
            </ProductActions>
        </ProductRoot>
    );
}

export default Product;
