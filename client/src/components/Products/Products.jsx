import styled from '@emotion/styled';
import { Grid, LinearProgress } from '@material-ui/core';

import qs from 'qs';
import React, { useEffect } from 'react';

import { connect } from 'react-redux';
import { useHistory } from 'react-router';
import { addToCart } from '../../store/cart/actions';
import { getProducts } from '../../store/products/actions';
import SearchField from '../Navbar/SearchField/SearchField';

import Product from './Product/Product';

const MainWrapper = styled.main`
  flex-grow: 1;
  padding: 3rem;
`;
const Toolbar = styled.div`
  height: 25px;
`;

const Products = ({
                      products,
                      getProducts,
                      loading,
                      addToCart
                  }) => {
    const history = useHistory();
    const searchText = qs.parse(history.location.search, {ignoreQueryPrefix: true}).search;

    useEffect(() => {
        getProducts(searchText);
    }, []);

    // Cart actions
    const addItem = (product, quantity = 1) => {
        addToCart({ product, quantity })
    };

    const handleSearch = (e) => {
        const searchText = e.target.value;
        if (searchText) {
            getProducts(searchText);
            history.push(`/products?search=${searchText}`);
        } else {
            // clear path
            getProducts();
            history.push('/products');
        }
    };

    return (
        <MainWrapper>
            <Toolbar/>
            {loading && <LinearProgress/>}
            <SearchField onInput={handleSearch} defaultValue={searchText}/>
            <Grid container justifyContent="center" spacing={4}>
                {products.map((product) => (
                    <Grid item key={product.id} xs={12} md={4} lg={3}>
                        <Product product={product} onAddToCart={addItem}/>
                    </Grid>
                ))}
            </Grid>
        </MainWrapper>
    );
};

const mapStateToProps = (state) => ({
    products: state.products.products,
    loading: state.products.loading
});

const mapDispatchToProps = {
    getProducts,
    addToCart
};

export default connect(mapStateToProps, mapDispatchToProps)(Products);
