import * as cartActions from './cart/actions';
import * as orderActions from './order/actions';
import * as ordersActions from './orders/actions';
import * as productsActions from './products/actions';
import * as userActions from './user/actions';

const actions = {
    cart: cartActions,
    order: orderActions,
    orders: ordersActions,
    products: productsActions,
    user: userActions,
};

export default actions;
