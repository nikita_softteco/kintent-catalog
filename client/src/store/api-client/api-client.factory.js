import {ApiClient} from "./api-client";

export const apiClient = (authToken) => new ApiClient(authToken);
