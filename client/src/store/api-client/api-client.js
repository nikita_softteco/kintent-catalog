import { BaseApiClient } from './base-api-client';

export class ApiClient extends BaseApiClient {
    constructor(authToken) {
        super(process.env.REACT_APP_API_URL);
        this.authToken = authToken;
    }

    defaultHeaders() {
        return {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${this.authToken}`
        };
    }

    login(credentials) {
        return this.post('auth/login', null, this.defaultHeaders(), credentials);
    }

    getProducts(searchText) {
        const query = searchText ? { search: searchText } : null;
        return this.get('products', query, this.defaultHeaders());
    }

    getOrders() {
        return this.get('orders', null, this.defaultHeaders());
    }

    getOrder(id) {
        return this.get(`orders/${id}`, null, this.defaultHeaders());
    }

    submitOrder(body) {
        return this.post('orders', null, this.defaultHeaders(), body);
    }

    cancelOrder(id) {
        return this.patch(`orders/${id}`, null, this.defaultHeaders());
    }
}
