import { ajax } from 'rxjs/ajax';
import qs from 'qs';
import { HTTP_METHODS } from '../../common/consts';

export class BaseApiClient {
    constructor(baseUrl) {
        this.baseUrl = baseUrl;
    }

    request(method, url, query, headers, body) {
        switch (method) {
            case HTTP_METHODS.GET:
            case HTTP_METHODS.DELETE:
                return ajax[method](this.composeUrl(url, query), headers);
            case HTTP_METHODS.POST:
            case HTTP_METHODS.PUT:
            case HTTP_METHODS.PATCH:
                return ajax[method](this.composeUrl(url, query), body, headers);
        }
    }

    get(url, query, headers) {
        return this.request(HTTP_METHODS.GET, url, query, headers);
    }

    post(url, query, headers, body) {
        return this.request(HTTP_METHODS.POST, url, query, headers, body);
    }

    put(url, query, headers, body) {
        return this.request(HTTP_METHODS.PUT, url, query, headers, body);
    }

    patch(url, query, headers, body) {
        return this.request(HTTP_METHODS.PATCH, url, query, headers, body);
    }

    delete(url, query, headers) {
        return this.request(HTTP_METHODS.DELETE, url, query, headers);
    }

    composeUrl(path, query) {
        const url = `${this.baseUrl}/${path}`;

        return query ? `${url}?${qs.stringify(query, { skipNulls: true })}` : url;
    }
}
