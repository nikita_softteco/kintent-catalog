import * as actionTypes from './types';

export const addToCart = (payload) => ({
    type: actionTypes.ADD_TO_CART,
    payload
});

export const removeFromCart = (payload) => ({
    type: actionTypes.REMOVE_FROM_CART,
    payload
});

export const emptyCart = (payload) => ({
    type: actionTypes.EMPTY_CART,
    payload
});

export const submitCart = (payload) => ({
    type: actionTypes.SUBMIT_CART,
    payload
});

