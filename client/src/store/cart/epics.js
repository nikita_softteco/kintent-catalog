import { ofType } from 'redux-observable';
import { mergeMap, of } from 'rxjs';
import { catchError, map, withLatestFrom } from 'rxjs/operators';
import { apiClient } from '../api-client/api-client.factory';
import * as actionTypes from './types';

export const submitCart = (action$, state$) => {
    return action$.pipe(
        ofType(actionTypes.SUBMIT_CART),
        withLatestFrom(state$),
        mergeMap(async ([, state]) => {
            return apiClient(state.user.token).submitOrder(state.cart.cart)
                .pipe(
                    map((payload) => ({
                        type: actionTypes.SUBMIT_CART_FULFILLED,
                        payload: { cart: payload.response.order }
                    })),
                    catchError((payload) => {
                        return of({
                            type: actionTypes.SUBMIT_CART_REJECTED,
                            payload
                        });
                    })
                ).toPromise();
        }),
    );
}
