import { createReducer, current } from '@reduxjs/toolkit';
import { STATUSES } from '../../common/consts';
import { addToItemToCart, removeItemFromCart } from '../utils/cart';
import * as actionTypes from './types';

const initialState = {
    cart: {
        total_sum: 0,
        order_items: []
    }
};

export default createReducer(initialState, {
    [actionTypes.ADD_TO_CART]: (state, action) => {
        const currentState = current(state);
        const { product, quantity } = action.payload

        return {
            ...currentState,
            ...addToItemToCart(product, quantity, currentState.cart),
        }
    },

    [actionTypes.REMOVE_FROM_CART]: (state, action) => {
        const currentState = current(state);

        return {
            ...currentState,
            ...removeItemFromCart(action.payload.product, currentState.cart)
        }
    },

    [actionTypes.EMPTY_CART]: () => initialState,

    // Submit order
    [actionTypes.SUBMIT_CART]: (state) => ({
        ...state,
        loading: true,
        status: STATUSES.PENDING,
    }),
    [actionTypes.SUBMIT_CART_FULFILLED]: (state) => ({
        ...state,
        loading: false,
        status: STATUSES.SUCCESS,
        ...initialState
    }),
    [actionTypes.SUBMIT_CART_REJECTED]: (state, action) => ({
        ...state,
        loading: false,
        status: STATUSES.FAILED,
        error: action.payload
    }),
});
