import { combineEpics } from 'redux-observable';

import * as cartEpics from './cart/epics';
import * as orderEpics from './order/epics';
import * as ordersEpics from './orders/epics';
import * as productsEpics from './products/epics';
import * as userEpics from './user/epics';

const epics = {
    ...cartEpics,
    ...orderEpics,
    ...ordersEpics,
    ...productsEpics,
    ...userEpics,
}

const epicValues = [];
Object.keys(epics).map(key => epicValues.push(epics[key]));

export default combineEpics(...epicValues);
