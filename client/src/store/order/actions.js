import * as actionTypes from './types';

export const getOrder = (payload) => ({
    type: actionTypes.GET_ORDER,
    payload
});

export const cancelOrder = (payload) => ({
    type: actionTypes.CANCEL_ORDER,
    payload
});
