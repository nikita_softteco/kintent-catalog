import { ofType } from 'redux-observable';
import { mergeMap, of } from 'rxjs';
import { catchError, map, withLatestFrom } from 'rxjs/operators';
import { apiClient } from '../api-client/api-client.factory';
import * as actionTypes from './types';

export const getOrder = (action$, state$) => {
    return action$.pipe(
        ofType(actionTypes.GET_ORDER),
        withLatestFrom(state$),
        mergeMap(async ([action, state]) => {
            return apiClient(state.user.token).getOrder(action.payload.id)
                .pipe(
                    map((payload) => ({
                        type: actionTypes.GET_ORDER_FULFILLED,
                        payload: { order: payload.response.order }
                    })),
                    catchError((payload) => {
                        return of({
                            type: actionTypes.GET_ORDER_REJECTED,
                            payload
                        });
                    })
                ).toPromise();
        }),
    );
}

export const cancelOrder = (action$, state$) => {
    return action$.pipe(
        ofType(actionTypes.CANCEL_ORDER),
        withLatestFrom(state$),
        mergeMap(async ([action, state]) => {
            return apiClient(state.user.token).cancelOrder(action.payload.id)
                .pipe(
                    map((payload) => ({
                        type: actionTypes.CANCEL_ORDER_FULFILLED,
                        payload: { status: payload.response.status }
                    })),
                    catchError((payload) => {
                        return of({
                            type: actionTypes.CANCEL_ORDER_REJECTED,
                            payload
                        });
                    })
                ).toPromise();
        }),
    );
}
