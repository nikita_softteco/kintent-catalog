import { createReducer, current } from '@reduxjs/toolkit';
import { ORDER_STATUSES, STATUSES } from '../../common/consts';
import * as actionTypes from './types';

const initialState = {
    order: null
}

export default createReducer(initialState, {
    // Get order
    [actionTypes.GET_ORDER]: (state) => ({
        ...state,
        loading: true,
        status: STATUSES.PENDING,
    }),
    [actionTypes.GET_ORDER_FULFILLED]: (state, action) => ({
        ...state,
        loading: false,
        status: STATUSES.SUCCESS,
        order: action.payload.order
    }),
    [actionTypes.GET_ORDER_REJECTED]: (state, action) => ({
        ...state,
        loading: false,
        status: STATUSES.FAILED,
        error: action.payload
    }),

    // Cancel order
    [actionTypes.CANCEL_ORDER]: (state) => ({
        ...state,
        loading: true,
        status: STATUSES.PENDING
    }),
    [actionTypes.CANCEL_ORDER_FULFILLED]: (state, action) => {
        const currentState = current(state);
        return {
            ...state,
            loading: false,
            status: STATUSES.SUCCESS,
            order: {
                ...currentState.order,
                status: action.payload.status === 'ok' ? ORDER_STATUSES.CANCELLED : currentState.order.status
            }
        }
    },
    [actionTypes.CANCEL_ORDER_REJECTED]: (state, action) => ({
        ...state,
        loading: false,
        status: STATUSES.FAILED,
        error: action.payload
    }),
})
