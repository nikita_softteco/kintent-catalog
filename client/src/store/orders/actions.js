import * as actionTypes from './types';

export const getOrders = (payload) => ({
    type: actionTypes.GET_ORDERS,
    payload
});
