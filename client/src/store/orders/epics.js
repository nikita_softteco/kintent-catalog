import { ofType } from 'redux-observable';
import { mergeMap, of } from 'rxjs';
import { catchError, map, withLatestFrom } from 'rxjs/operators';
import { apiClient } from '../api-client/api-client.factory';
import * as actionTypes from './types';

export const getOrders = (action$, state$) => {
    return action$.pipe(
        ofType(actionTypes.GET_ORDERS),
        withLatestFrom(state$),
        mergeMap(async ([, state]) => {
            return apiClient(state.user.token).getOrders()
                .pipe(
                    map((payload) => ({
                        type: actionTypes.GET_ORDERS_FULFILLED,
                        payload: { orders: payload.response.orders }
                    })),
                    catchError((payload) => {
                        return of({
                            type: actionTypes.GET_ORDERS_REJECTED,
                            payload
                        });
                    })
                ).toPromise();
        }),
    );
}
