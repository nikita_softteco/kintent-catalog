import { createReducer } from '@reduxjs/toolkit';
import { STATUSES } from '../../common/consts';
import * as actionTypes from './types';

const initialState = {
    orders: []
};

export default createReducer(initialState, {
    [actionTypes.GET_ORDERS]: (state) => ({
        ...state,
        loading: true,
        status: STATUSES.PENDING,
    }),
    [actionTypes.GET_ORDERS_FULFILLED]: (state, action) => ({
        ...state,
        loading: false,
        status: STATUSES.SUCCESS,
        orders: action.payload.orders
    }),
    [actionTypes.GET_ORDERS_REJECTED]: (state, action) => ({
        ...state,
        loading: false,
        status: STATUSES.FAILED,
        orders: action.payload
    }),
})
