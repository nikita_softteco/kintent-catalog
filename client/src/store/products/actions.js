import * as actionTypes from './types';

export const getProducts = (payload) => ({
    type: actionTypes.GET_PRODUCTS,
    payload
});
