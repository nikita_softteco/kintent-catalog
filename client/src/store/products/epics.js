import { ofType } from 'redux-observable';
import { mergeMap, of } from 'rxjs';
import { map, catchError, withLatestFrom } from 'rxjs/operators';

import * as actionTypes from './types';
import { apiClient } from '../api-client/api-client.factory';

export const getProducts = (action$, state$) => {
    return action$.pipe(
        ofType(actionTypes.GET_PRODUCTS),
        withLatestFrom(state$),
        mergeMap(async ([action, state]) => {
            return apiClient(state.user.token).getProducts(action.payload)
                .pipe(
                    map((payload) => ({
                        type: actionTypes.GET_PRODUCTS_FULFILLED,
                        payload: { products: payload.response.products }
                    })),
                    catchError((payload) => {
                        return of({
                            type: actionTypes.GET_PRODUCTS_REJECTED,
                            payload
                        });
                    })
                ).toPromise();
        }),
    );
}
