import * as actionTypes from './types';
import { createReducer } from '@reduxjs/toolkit';
import { STATUSES } from '../../common/consts';

const initialState = {
    products: []
};

export default createReducer(initialState, {
    [actionTypes.GET_PRODUCTS]: (state) => ({
        ...state,
        loading: true,
        status: STATUSES.PENDING,
    }),
    [actionTypes.GET_PRODUCTS_FULFILLED]: (state, action) => ({
        ...state,
        loading: false,
        status: STATUSES.SUCCESS,
        products: action.payload.products
    }),
    [actionTypes.GET_PRODUCTS_REJECTED]: (state, action) => ({
        ...state,
        loading: false,
        status: STATUSES.FAILED,
        error: action.payload
    }),
})
