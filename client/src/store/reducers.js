import { combineReducers } from '@reduxjs/toolkit';
import { routerReducer } from 'react-router-redux';

import cartReducer from './cart/reducers';
import orderReducer from './order/reducers';
import ordersReducer from './orders/reducers';
import productsReducer from './products/reducers';
import userReducer from './user/reducers';

const rootReducer = combineReducers({
    routing: routerReducer,
    cart: cartReducer,
    order: orderReducer,
    orders: ordersReducer,
    products: productsReducer,
    user: userReducer,
});

export default rootReducer;
