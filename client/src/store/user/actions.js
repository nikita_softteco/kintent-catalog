import * as actionTypes from './types';

export const login = (payload) => ({
    type: actionTypes.LOGIN,
    payload
});
