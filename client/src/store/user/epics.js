import { ofType } from 'redux-observable';
import { mergeMap, of } from 'rxjs';
import { catchError, map, withLatestFrom } from 'rxjs/operators';
import { apiClient } from '../api-client/api-client.factory';

import * as actionTypes from './types';

export const login = (action$, state$) => {
    return action$.pipe(
        ofType(actionTypes.LOGIN),
        withLatestFrom(state$),
        mergeMap(async ([action]) => {
            return apiClient().login(action.payload)
                .pipe(
                    map((payload) => ({
                        type: actionTypes.LOGIN_FULFILLED,
                        payload: { token: payload.response.access_token }
                    })),
                    catchError((payload) => {
                        return of({
                            type: actionTypes.LOGIN_REJECTED,
                            payload
                        });
                    })
                ).toPromise()
        })
    );
}
