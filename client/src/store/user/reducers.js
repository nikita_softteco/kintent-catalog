import { createReducer } from '@reduxjs/toolkit';
import { STATUSES } from '../../common/consts';
import * as actionTypes from './types';

const initialState = {
    token: '',
}

export default createReducer(initialState, {
    [actionTypes.LOGIN]: (state) => ({
        ...state,
        loading: true,
        status: STATUSES.PENDING,
    }),
    [actionTypes.LOGIN_FULFILLED]: (state, action) => ({
        ...state,
        loading: false,
        status: STATUSES.SUCCESS,
        token: action.payload.token
    }),
    [actionTypes.LOGIN_REJECTED]: (state, action) => ({
        ...state,
        loading: false,
        status: STATUSES.FAILED,
        error: action.payload
    }),
});
