const toCents = (value) => {
    return typeof value === 'number' ? Math.round(100 * value) : null;
}

const fromCents = (value) => {
    return typeof value === 'number' ? value / 100 : null;
}

export const calculateTotalSum = (items) => {
    return items.reduce(
        (accumulator, currentValue) => {
            const acc = typeof accumulator === 'number' ? accumulator : (accumulator.product.price * accumulator.quantity);
            return fromCents(toCents(acc) + toCents(currentValue.product.price * currentValue.quantity));
        }, 0);
}

export const addToItemToCart = (product, quantity, currentCart) => {
    // Check if Item is in cart already
    let orderItems;
    const item = currentCart.order_items.find((itm) => itm.product_id === product.id);
    if (item) {
        orderItems = currentCart.order_items.map((itm) => ({
            product_id: itm.product_id,
            product: itm.product,
            quantity: itm.product_id === product.id ? +itm.quantity + quantity : itm.quantity
        }))
    } else {
        orderItems = [...currentCart.order_items, {
            product_id: product.id,
            product,
            quantity
        }];
    }

    const totalSum = calculateTotalSum(orderItems);

    return {
        cart: {
            total_sum: totalSum,
            order_items: orderItems,
        },
    };
}

export const removeItemFromCart = (product, cart) => {
    const orderItems = cart.order_items.filter((item) => item.product_id !== product.id);

    return {
        cart: {
            total_sum: calculateTotalSum(orderItems),
            order_items: orderItems
        }
    };
}
